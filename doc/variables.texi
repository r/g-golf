@c -*-texinfo-*-
@c This is part of the GNU G-Golf Reference Manual.
@c Copyright (C) 2016 - 2023 Free Software Foundation, Inc.
@c See the file g-golf.texi for copying conditions.


@set TITLE GNU G-Golf Reference Manual

@set MANUAL-REVISION 1

@set UGUILE http://www.gnu.org/software/guile
@set UGUILE-ENV-VARS https://www.gnu.org/software/guile/manual/guile.html#Environment-Variables
@set UGUILE-LOAD-PATH https://www.gnu.org/software/guile/manual/guile.html#Load-Paths
@set UGUILE-FOREIGN-TYPES https://www.gnu.org/software/guile/manual/guile.html#Foreign-Types
@set UGUILE-LISTINFO https://lists.gnu.org/mailman/listinfo/

@set UGUILE-LIB http://www.nongnu.org/guile-lib
@set USCHEME http://schemers.org

@set UGUILE-CAIRO http://www.nongnu.org/guile-cairo

@set UG-GOLF http://www.gnu.org/software/g-golf/
@set UG-GOLF-learn https://www.gnu.org/software/g-golf/learn.html
@set UG-GOLF-RELEASES http://ftp.gnu.org/gnu/g-golf/
@set UG-GOLF-LATEST http://ftp.gnu.org/gnu/g-golf/g-golf-@value{VERSION}.tar.gz
@set UG-GOLF-SAVANNAH https://savannah.gnu.org/projects/g-golf
@set UG-GOLF-LISTINFO https://lists.gnu.org/mailman/listinfo/g-golf-user
@set UG-GOLF-BUGS-TRACKER https://savannah.gnu.org/bugs/?group=g-golf
@set UG-GOLF-BUGS-LISTINFO https://lists.gnu.org/mailman/listinfo/bug-g-golf
@set UG-GOLF-GIT http://git.savannah.gnu.org/cgit/g-golf.git

@set UDEBBUGS-SERVER-CONTROL https://debbugs.gnu.org/server-control.html

@set UGRIP http://www.nongnu.org/grip/index.html
@set UGRIP-GNOME http://www.nongnu.org/grip/index.html
@set UGRIP-SQLITE http://www.nongnu.org/grip/index.html

@set UGIT https://git-scm.com/

@set USBANK http://rotty.xx.vu/software/sbank/

@set UGNOME https://www.gnome.org/

@set UGI-wiki https://wiki.gnome.org/Projects/GObjectIntrospection
@set UGI https://developer.gnome.org/stable/gi
@set UGI-OVERVIEW https://gi.readthedocs.io/en/latest
@set UGIRepository https://developer.gnome.org/gi/stable/GIRepository.html
@set UGIcommontypes https://developer.gnome.org/gi/stable/gi-common-types.html
@set UGIBaseInfo https://developer.gnome.org/gi/stable/gi-GIBaseInfo.html
@set UGICallableInfo https://developer.gnome.org/gi/stable/gi-GICallableInfo.html
@set UGIFunctionInfo https://developer.gnome.org/gi/stable/gi-GIFunctionInfo.html
@set UGICallbackInfo https://developer.gnome.org/gi/stable/gi-GICallbackInfo.html
@set UGISignalInfo https://developer.gnome.org/gi/stable/gi-GISignalInfo.html
@set UGIVFuncInfo https://developer.gnome.org/gi/stable/gi-GIVFuncInfo.html
@set UGIRegisteredTypeInfo https://developer.gnome.org/gi/stable/gi-GIRegisteredTypeInfo.html
@set UGIEnumInfo https://developer.gnome.org/gi/stable/gi-GIEnumInfo.html
@set UGIStructInfo https://developer.gnome.org/gi/stable/gi-GIStructInfo.html
@set UGIUnionInfo https://developer.gnome.org/gi/stable/gi-GIUnionInfo.html
@set UGIObjectInfo https://developer.gnome.org/gi/stable/gi-GIObjectInfo.html
@set UGIInterfaceInfo https://developer.gnome.org/gi/stable/gi-GIInterfaceInfo.html
@set UGIArgInfo https://developer.gnome.org/gi/stable/gi-GIFieldInfo.html
@set UGIConstantInfo https://developer.gnome.org/gi/stable/gi-GIInterfaceInfo.html
@set UGIFieldInfo https://developer.gnome.org/gi/stable/gi-GIFieldInfo.html
@set UGIPropertyInfo https://developer.gnome.org/gi/stable/gi-GIPropertyInfo.html
@set UGITypeInfo https://developer.gnome.org/gi/stable/gi-GITypeInfo.html
@set UGIValueInfo https://developer.gnome.org/gi/stable/gi-GIValueInfo.html

@set UGNOME-Libraries https://developer.gnome.org/documentation/introduction/overview/libraries.html

@set ULIBADWAITA https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/
@set UGNOME-Web https://wiki.gnome.org/Apps/Web

@set UGLIB https://developer.gnome.org/glib/stable/
@set UGLIB-Mem-Alloc https://developer.gnome.org/glib/stable/glib-Memory-Allocation.html
@set UGLIB-Main-Event-Loop https://developer.gnome.org/glib/stable/glib-The-Main-Event-Loop.html
@set UGLIB-IO-Channels https://developer.gnome.org/glib/stable/glib-IO-Channels.html
@set UGLIB-GList https://developer.gnome.org/glib/stable/glib-Doubly-Linked-Lists.html
@set UGLIB-GSList https://developer.gnome.org/glib/stable/glib-Singly-Linked-Lists.html
@set UGLIB-Markup https://docs.gtk.org/glib/markup.html

@set UGOBJECT-Tutorial https://docs.gtk.org/gobject/tutorial.html

@set UGOBJECT https://developer.gnome.org/gobject/stable/
@set UGOBJECT-Introduction https://developer.gnome.org/gobject/stable/pr01.html
@set UGOBJECT-Concepts https://docs.gtk.org/gobject/concepts.html
@set UGOBJECT-Concepts-GType https://developer.gnome.org/gobject/stable/chapter-gtype.html
@set UGOBJECT-Concepts-GObject https://developer.gnome.org/gobject/stable/chapter-gobject.html
@set UGOBJECT-Concepts-Signals https://developer.gnome.org/gobject/stable/chapter-signal.html
@set UGOBJECT-Class-Hierarchy https://docs.gtk.org/gobject/classes_hierarchy.html

@set UGOBJECT-Type-Info https://developer.gnome.org/gobject/stable/gobject-Type-Information.html
@set UGOBJECT-GType-Module https://developer.gnome.org/gobject/stable/GTypeModule.html
@set UGOBJECT-GObject https://developer.gnome.org/gobject/stable/gobject-The-Base-Object-Type.html
@set UGOBJECT-GObject-struct https://developer.gnome.org/gobject/stable/gobject-The-Base-Object-Type.html#GObject-struct
@set UGOBJECT-Enum-Flags https://developer.gnome.org/gobject/stable/gobject-Enumeration-and-Flag-Types.html
@set UGOBJECT-Boxed-Types https://developer.gnome.org/gobject/stable/gobject-Boxed-Types.html
@set UGOBJECT-Gen-Vals https://developer.gnome.org/gobject/stable/gobject-Generic-Values.html
@set UGOBJECT-Params-Vals https://developer.gnome.org/gobject/stable/gobject-Parameters-and-Values.html
@set UGOBJECT-GParamSpec https://developer.gnome.org/gobject/stable/gobject-GParamSpec.html
@set UGOBJECT-Closures https://developer.gnome.org/gobject/stable/gobject-Closures.html
@set UGOBJECT-Signals https://developer.gnome.org/gobject/stable/gobject-Signals.html
@set UGOBJECT-Object-add-toggle-ref https://docs.gtk.org/gobject/method.Object.add_toggle_ref.html

@c
@c Clutter-1.0
@c
@set UCLUTTER http://blogs.gnome.org/clutter/


@c
@c Gtk-3.0, Gdk-3.0
@c
@set UGTK3 https://developer.gnome.org/gtk3/stable

@set UGDK3 https://developer.gnome.org/gdk3/stable
@set UGDK-Events https://developer.gnome.org/gdk3/stable/gdk3-Events.html
@set UGDK-ModifierType https://developer.gnome.org/gdk3/stable/gdk3-Windows.html#GdkModifierType
@set UGDK-KeyValues https://developer.gnome.org/gdk3/stable/gdk3-Keyboard-Handling.html


@c
@c Gtk-4.0, Gdk-4.0, Gsk-4.0
@c

@set UGTK-MIGRATING-3-TO-4 https://developer.gnome.org/gtk4/stable/gtk-migrating-3-to-4.html

@set UGTK-WEB https://gtk.org/
@set UGTK4 https://docs.gtk.org/gtk4/index.html
@set UGTK4-APPLICATION https://docs.gtk.org/gtk4/class.Application.html
@set UGDK4 https://docs.gtk.org/gdk4/index.html
@set UGSK4 https://docs.gtk.org/gsk4/index.html


@c
@c Adw-1
@c

@set UADW1 https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/

@c
@c Gio
@c

@set UGIO https://developer.gnome.org/gio/stable/index.html
@set UGIO-G-APPLICATION https://developer.gnome.org/gio/stable/GApplication.html
@set UGIO-G-APPLICATION-IS-VALID https://developer.gnome.org/gio/stable/GApplication.html#g-application-id-is-valid




@c
@c HowDoI
@c

@set UHDI https://wiki.gnome.org/HowDoI
@set UHDI-GTKAPPLICATION https://wiki.gnome.org/HowDoI/GtkApplication
@set UHDI-GTKAPPLICATION-CMDLINE https://wiki.gnome.org/HowDoI/GtkApplication/CommandLine

@c GG -> Guile-Gnome
@set UGG https://www.gnu.org/software/guile-gnome

@c GGMGOU Guile-Gnome Manual GObject Utils
@set UGGMGOU https://www.gnu.org/software/guile-gnome/docs/gobject/html/gnome-gobject-utils.html#gnome-gobject-utils

@c GGMGOG Guile-Gnome Manual GObject Generics
@set UGGMGOG https://www.gnu.org/software/guile-gnome/docs/gobject/html/gnome-gobject-generics.html#gnome-gobject-generics

@set UWIKI-CAMELCASE https://en.wikipedia.org/wiki/Camel_case


@c
@c Freedesktop.org
@c

@c XDG Base Directory Specification
@set UXDG-BASEDIR-SPEC http://www.freedesktop.org/Standards/basedir-spec


@c
@c GNU
@c

@set UGNU-Project http://www.gnu.org/
@set UGNU-PHILOSOPHY https://gnu.org/philosophy/free-sw.html
@set UGNU-EVALUATION https://www.gnu.org/help/evaluation.html
@set UGNU-HELP https://www.gnu.org/help/help.html


@c
@c Color module inspired by
@c

@set UCOLOR-Chickadee https://git.dthompson.us/chickadee/tree/chickadee/graphics/color.scm
@set UCOLOR-Colorways https://github.com/jeremymadea/colorways

@c
@c Additional color palettes
@c

@set UPANGO-Palette https://gitlab.gnome.org/GNOME/pango/-/blob/main/tools/rgb.txt
@set UCSS-Palette https://www.w3.org/TR/css-color-4/#color-keywords
@set UTANGO-Palette https://en.wikipedia.org/wiki/Tango_Desktop_Project#Palette
@set UDB32-Palette http://pixeljoint.com/forum/forum_posts.asp?TID=16247
@set UDB32-Color-Names http://privat.bahnhof.se/wb364826/pic/db32.gpl

@set UBLEND-Modes https://en.wikipedia.org/wiki/Blend_modes
