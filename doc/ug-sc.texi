@c -*-texinfo-*-
@c This is part of the GNU G-Golf Reference Manual.
@c Copyright (C) 2024 Free Software Foundation, Inc.
@c See the file g-golf.texi for copying conditions.


@node Support Canyon
@section Support Canyon


@menu
* Color Hollow:: A color management module.
* Utils Corner:: Some utilities.
@end menu


@include ug-sc-color.texi
@include ug-sc-utils.texi
